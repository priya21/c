﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
            GridBind();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        SqlConnection conn = new SqlConnection(Helper.ConnectionString);
        string Fname = txtFirstName.Text.Replace("'", "''");
        string Lname = txtLastName.Text.Replace("'", "''");
        int zipcode = int.Parse(txtZipCode.Text);
        string City = txtCity.Text.Replace("'", "''");
        string State = txtState.Text.Replace("'", "''");
        SqlCommand cmd = new SqlCommand("Insert into ContactDetails(FirstName,LastName,ZipCode,City,State)values('" + Fname + "','" + Lname + "'," + zipcode + ",'" + City + "','" +  State + "')", conn);
        conn.Open();
        cmd.ExecuteNonQuery();
        conn.Close();
        GridBind();
    }
    private void GridBind()
    {
        SqlConnection conn = new SqlConnection(Helper.ConnectionString);
        SqlDataAdapter da;
        DataSet ds;

        da = new SqlDataAdapter("Select ContactId,FirstName,LastName,ZipCode,City,State From ContactDetails", conn);
        ds = new DataSet();
        da.Fill(ds, "Contacts");
        GrdCD.DataSource = ds.Tables[0];
        GrdCD.DataBind();
    }


    protected void GrdCD_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName == "Edit")
        //{
        //    int indRow = Convert.ToInt32(e.CommandArgument);
        //    GridViewRow gvr = GrdCD.Rows[indRow];
        //    Response.Write(gvr.Cells[1].Text);
        //}
    }
    protected void GrdCD_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GrdCD.EditIndex = e.NewEditIndex;
        GridBind();
    }
    protected void GrdCD_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GrdCD.EditIndex = -1;
        GridBind();
    }
    protected void GrdCD_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        SqlConnection conn = new SqlConnection(Helper.ConnectionString);
        int Id = Convert.ToInt32(GrdCD.DataKeys[e.RowIndex].Value.ToString());
        GridViewRow row = (GridViewRow)GrdCD.Rows[e.RowIndex];
        //Label llID = (Label)row.FindControl("lblID");
        TextBox txtFName = (TextBox)row.Cells[1].Controls[0];
        TextBox txtLName = (TextBox)row.Cells[2].Controls[0];
        //TextBox txtZip = (TextBox)row.Cells[3].Controls[0];
        TextBox txtZip = (TextBox)row.FindControl("txtZipEdit");
        //TextBox txtCityEdit = (TextBox)row.Cells[4].Controls[0];
        TextBox txtCityEdit = (TextBox)row.FindControl("txtCityCol");
        //TextBox txtStateEdit = (TextBox)row.Cells[5].Controls[0];
        TextBox txtStateEdit = (TextBox)row.FindControl("txtStateCol");
        GrdCD.EditIndex = -1;
        conn.Open();
        SqlCommand cmd = new SqlCommand("update ContactDetails set Firstname='" + txtFName.Text + "',LastName='" + txtLName.Text + "',ZipCode=" + txtZip.Text + ",City='" + txtCityEdit.Text + "',State='" + txtStateEdit.Text + "'where ContactId=" + Id, conn);
        cmd.ExecuteNonQuery();
        conn.Close();
        GridBind();

    }
}