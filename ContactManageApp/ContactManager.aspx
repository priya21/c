﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContactManager.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
        function GetCityState(zip,mode) {
            //var zip = document.forms[0].elements["txtZipCode"].value;
            var client = new XMLHttpRequest();
            client.open("GET", "http://ziptasticapi.com/"+zip.value, true);
            client.onreadystatechange = function () {
                if (client.readyState == 4) {
                   // alert(client.responseText);
                    var state = client.responseText.substring(25, 27);
                    var city = client.responseText.substring(37);
                    var newcity = city.substring(0,city.length -2);
                    if (mode == 1) {
                        document.forms[0].elements["txtState"].value = state;
                        document.forms[0].elements["txtCity"].value = newcity;
                    }
                    else if (mode == 2) {
                        var grd = document.getElementById('<%= GrdCD.ClientID %>');
                        for (var rowId = 1; rowId < grd.rows.length; rowId++) {
                            var grdCity = grd.rows[rowId].cells[3].children[0];
                            var grdState = grd.rows[rowId].cells[4].children[0];
                            grdCity.value = newcity;
                            grdState.value = state;
                        }
                    }

                };
            };
            client.send();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table>
            <tr>
                 <td>
                    <asp:Label ID="lblFirstName" Text="First Name" runat="server" />
                </td>
                <td>
                    <%--<input type="text" id="txtFirstName" value="" />--%>
                    <asp:TextBox ID="txtFirstName" runat="server" />
                </td>
            </tr>
            <tr>
                 <td>
                    <asp:Label ID="lblLastName" Text="Last Name" runat="server" />
                </td>
                <td>
                    <%--<input type="text" id="txtLastName" value="" />--%>
                    <asp:TextBox ID="txtLastName" runat="server" />
                </td>
            </tr>
            <tr>
                 <td>
                    <asp:Label ID="lblZipCode" Text="Zip Code" runat="server" />
                </td>
                <td>
                    <%--<input type="text" id="txtZipCode" onchange="GetCityState()"  />--%>
                    <asp:TextBox ID="txtZipCode" runat="server" OnChange="GetCityState(this,1)"/>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCity" Text="City" runat="server" />
                </td>
                <td>
                    <%--<input type="text" id="txtCity" value="" />--%>
                    <asp:TextBox ID="txtCity" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblState" Text="State" runat="server" />
                </td>
                <td>
                    <%--<input type="text" id="txtState" value="" />--%>
                    <asp:TextBox ID="txtState" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                </td>
            </tr>
        </table>
    

    <div>

        <asp:GridView ID="GrdCD" runat="server" CellPadding="4" ForeColor="#333333" 
            GridLines="None" AutoGenerateColumns="False" OnRowCommand="GrdCD_RowCommand" 
            OnRowCancelingEdit="GrdCD_RowCancelingEdit" OnRowEditing="GrdCD_RowEditing" 
            OnRowUpdating="GrdCD_RowUpdating" DataKeyNames="ContactId" EnableViewState="False">
            <AlternatingRowStyle BackColor="White"  />

            <Columns>
                <asp:BoundField DataField="ContactId" Visible="False" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                <asp:TemplateField HeaderText="Zip Code">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtZipEdit" runat="server" Text='<%# Eval("ZipCode") %>' onChange="GetCityState(this,2)" ></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("ZipCode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="City">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtCityCol" runat="server" Text='<%# Eval("City") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("City") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="State">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtStateCol" runat="server" Text='<%# Eval("State") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("State") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowEditButton="true" />
            </Columns>

            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />

        </asp:GridView>
         
    </div>
    </form>
    </body>
        
</html>
